import me.jfenn.gitrest.GitrestClient
import me.jfenn.gitrest.provider.gitea.GiteaProvider
import me.jfenn.gitrest.provider.github.GithubProvider
import me.jfenn.gitrest.provider.gitlab.GitlabProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.html.InputType
import kotlinx.html.button
import kotlinx.html.dom.append
import kotlinx.html.js.*
import me.jfenn.gitrest.gitrest
import org.w3c.dom.HTMLInputElement
import kotlin.browser.document

val client = gitrest {
    logDebug = { console.log(it) }
}

fun main() {
    document.body!!.append {
        div {
            h1 { +"Git-REST API Sample" }
            p {
                +"Enter a repo identifier..."
            }
            input {
                type = InputType.text
                placeholder = "gitea@code.horrific.dev:james/git-rest-wrapper"
            }
            button {
                +"Submit"
                onClickFunction = {
                    val element = document.getElementsByTagName("input").item(0) as? HTMLInputElement
                    element?.value?.let {
                        GlobalScope.launch { fetchRepo(it) }
                    }
                }
            }
        }
    }
}

suspend fun fetchRepo(id: String) = client.getRepo(id)?.let { repo ->
    document.body!!.append {
        div {
            h3 {
                +(repo.id)
            }
            p {
                repo.url?.let { url ->
                    a {
                        +"Repository"
                        href = url
                    }
                }
                +" | "
                repo.websiteUrl?.let { url ->
                    a {
                        +"Project Website"
                        href = url
                    }
                }
            }
            code {
                pre {
                    +("git clone ")
                    +(repo.gitUrlSsh ?: repo.gitUrlHttp ?: "")
                }
            }
            repo.description?.let {
                p {
                    +(it)
                }
            }
            p {
                +"Released under "
                +(repo.license?.name ?: "No License In Particular")
            }
        }
    }
}
