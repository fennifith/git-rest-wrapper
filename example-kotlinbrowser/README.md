# example-kotlinbrowser

This is an example of the library being used in another Kotlin/JS project to build a simple webpage.

- `./gradlew :example-web:run`: build and serve the website locally using Webpack
