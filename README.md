Git-REST API Wrapper [![NPM](https://img.shields.io/npm/v/git-rest-wrapper?color=red&logo=npm)](https://www.npmjs.com/package/git-rest-wrapper) [![JitPack](https://jitpack.io/v/dev.horrific.code.james/git-rest-wrapper.svg)](https://jitpack.io/#dev.horrific.code.james/git-rest-wrapper) [![Discord](https://img.shields.io/discord/514625116706177035.svg?logo=discord&colorB=7289da)](https://discord.jfenn.me/)
=====

This is a cross-platform API wrapper for GitHub, GitLab, and Gitea, in an attempt to normalize their API endpoints and promote interoperability. It is being written in [Kotlin Multiplatform](https://kotlinlang.org/docs/reference/multiplatform.html) with [ktor](https://ktor.io/), and aims to target the JVM, Android, JS, and native platforms - in other words, (almost) everything that ktor supports.

## Installation

### JavaScript

JavaScript projects can use the library through either the [NPM package](https://www.npmjs.com/package/git-rest-wrapper) or by using a compiled bundle hosted on the [UNPKG CDN](https://unpkg.com/browse/git-rest-wrapper/bundle/).

#### NPM Module

```shell script
npm install git-rest-wrapper
```

#### Bundled JS

```html
<script type="text/javascript" src="https://unpkg.com/git-rest-wrapper/bundle/gitrest.js"></script>
```

### Gradle / Java

The `:gitrest` module is published on [JitPack](https://jitpack.io/), which you can add to your project by copying the following to your root build.gradle at the end of "repositories".

```groovy
allprojects {
  repositories {
    ...
    maven { url 'https://jitpack.io' }
  }
}
```

To add the dependency to a module, copy this line into your app's build.gradle file.

```groovy
implementation "dev.horrific.code.james.git-rest-wrapper:gitrest-jvm:$gitrest_version"
```

The Android dependency can be imported similarly, using the root `gitrest` dependency instead of `gitrest-jvm`; gradle should identify & download the `gitrest-android` variant automatically.

```groovy
implementation "dev.horrific.code.james.git-rest-wrapper:gitrest:$gitrest_version"
```

#### Note: fixing duplicate META-INF files

Kotlin Multiplatform has a weird issue with dependency management that I haven't quite worked out; (#5)[https://code.horrific.dev/james/git-rest-wrapper/issues/5] documents some of my encounters with it. Hopefully you won't encounter this problem at all, but if you do, it should be enough to just add the following to your Android modules:

```groovy
android {
    ...
    packagingOptions {
        merge 'META-INF/*.kotlin_module'
    }
}
``` 

For any similar errors, adding a line to either `merge` or `exclude 'META-INF/<file>'` should do the trick.

## Usage

### JavaScript

After installing the module through NPM, the API wrapper can be used as detailed in the code snippet below.

```js
const gitrest = require('git-rest-wrapper');
const provider = new gitrest.Client();

provider.getUser("fennifith").then((user) => console.log(user.name));
```

The bundled JS file works the same as the NPM module - except the `gitrest` object will be available in the global scope.

```js
const provider = new gitrest.Client();

provider.getUser("fennifith").then((user) => console.log(user.name));
```

### Kotlin

In Kotlin, you'll need to import `me.jfenn.gitrest.gitrest` and instantiate it as follows (adding or removing providers as you see fit).

```kotlin
val client = gitrest {
    providers = arrayOf(GithubProvider, GitlabProvider, GiteaProvider)
    cache = MemoryCache()
}
```

You can optionally add API keys per-domain to any of these providers by using the `tokens` Map as follows:

```kotlin
GiteaProvider.apply {
    tokens["code.horrific.dev"] = "abcxyz"
}
```

Endpoints can then be accessed using [Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)...

```kotlin
GlobalScope.launch {
    val repo = client.getRepo("gitea@code.horrific.dev:james/git-rest-wrapper")
    println(repo?.description)
}
```

### Example Projects

- [Kotlin/JS - Browser](./example-kotlinbrowser)
- [NodeJS](./example-nodejs)
- [JS (Bundle) - Browser](./example-js)
- [JVM](./example-jvm)
- [Android](./example-android)

## Development

The project uses Gradle builds for... err... most things. `./gradlew :{module}:test` and `./gradle :{module}:run` are fairly universal.

The `:gitrest:jsBrowserTest` task seems to fail sporadically trying to parse... `/etc/fonts/fonts.conf`? Well, it spits out a lot of logs about it, then fails to actually run any of its tests, so it's not doing very much. It's excluded by default from most gradle tasks.

### Specification

#### URI Scheme

The library relies on its own identifier format similar to the URI syntax used in SSH - each identifier should be structured as `provider@hostname:id`.

- `provider` represents the implementation that should be used for the endpoint, e.g. `gitlab` or `gitea`. If omitted, the wrapper will query various endpoints in an attempt to determine the correct implementation before making the request.
- `hostname` represents the host/domain of the server that should be queried. If omitted, the implementation is left to the provider - `github:fennifith/Attribouter` will default to `github.com`, for example.
- `id` represents the resource identifier of the item being fetched, specific to the endpoint used.

The `provider` and `hostname` values are optional when they can be supplemented by the values in the [`DEFAULT_PROVIDERS`](https://code.horrific.dev/james/git-rest-wrapper/src/branch/main/gitrest/src/commonMain/kotlin/me/jfenn/gitrest/util/DefaultProviders.kt) array. This is also used to "correct" hostnames in cases where the API is served from a different domain than the one provided - for example, `github@github.com:some/repo` will resolve to `github@api.github.com:some/repo`. For compatibility reasons, `github@anything.can.go.here.github.com:some/repo` will exhibit the same behavior.

#### Endpoints

- `getUser(login)`: `login` is the username of the requested user.
- `getRepo(repo)`: `repo` is the repository "path", i.e. `{user}/{repo}` - `james/git-rest-wrapper`, for example.
- `getRepoContributors(repo)`: `repo` is the repository "path", same as `getRepo`
- `getLicense(key)`: `key` is a short license identifier - refer to [SPDX](https://spdx.org/licenses/) for a complete list (dependent on the server's implementation)

#### Models

Not all data is complete for every provider/context. For example, Gitea doesn't provide any license information in their Repository model - so that data will obviously be missing.

Each data model contains `id`, `context`, and `provider` values with respect to the corresponding portions of the URI.

- `User`
  - `id`: The username.
  - `name`: The full name of the user (defaults to `login` if unspecified).
  - `url`: The webpage / user-facing link to the user's profile.
  - `avatarUrl`: A link to the user's avatar / profile picture.
  - `websiteUrl`: The user's website / blog / external profile link.
  - `email`: The user's public email address.
  - `bio`: The user bio.
- `Repo`
  - `id`: The repository slug identifier, i.e. `james/git-rest-wrapper`
  - `description`: The repository/project description.
  - `url`: The webpage / user-facing repository link.
  - `websiteUrl`: The project website or homepage.
  - `gitUrlHttp`: HTTP url for `git clone`
  - `gitUrlSsh`: SSH uri for `git clone`
  - `license`: Repository `License` object - may be incomplete/missing, depending on implementation.
  - `defaultBranch`: The default branch of the repository.
  - `getRawFileUrl(branchName, filePath)`: endpoint used to access a raw file in the repository
- `License`
  - `id`: The license identifier/key, according to SPDX
  - `name`: A full user-facing license name (defaults to `key` if unspecified)
  - `description`: A short summary / description of the license.
  - `body`: The full license text.
  - `infoUrl`: A user-facing link to license info (ex: [choosealicense.com/licenses/mpl-2.0](https://choosealicense.com/licenses/mpl-2.0/))
  - `permissions`: An array of license "permissions", ex. `["commercial-use", "distribution", "modification"]`
  - `conditions`: An array of license "conditions", ex. `["disclose-source", "same-license"]`
  - `limitations`: An array of license "limitations", ex. `["liability", "warranty"]`  
