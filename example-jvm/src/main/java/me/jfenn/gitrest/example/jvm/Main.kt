package me.jfenn.gitrest.example.jvm

import me.jfenn.gitrest.gitrest

val client = gitrest {
    logDebug = { println(it) }
}

suspend fun fetchRepository(repoId: String) {
    client.getRepo(repoId)?.let {
        println("""
                ${it.id}:
                  ${it.description}
                  ${it.url}
                  ${it.websiteUrl}
            """.trimIndent())
    } ?: run {
        println("ERROR: failed to fetch repo $repoId")
    }
}

suspend fun main() {
    fetchRepository("gitea@code.horrific.dev:james/git-rest-wrapper")
    fetchRepository("github:fennifith/Attribouter")
    fetchRepository("gitlab@salsa.debian.org:reproducible-builds/reproducible-website")
}
