package me.jfenn.gitrest.example.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.jfenn.androidutils.bind
import me.jfenn.gitrest.GitrestClient
import me.jfenn.gitrest.gitrest
import me.jfenn.gitrest.service.DiskCache
import me.jfenn.gitrest.service.MemoryCache

class MainActivity : AppCompatActivity() {

    val recycler: RecyclerView? by bind(R.id.recycler)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val client = gitrest {
            cache = MemoryCache(DiskCache(this, cacheDir))
            logDebug = { println(it) }
        }

        recycler?.layoutManager = LinearLayoutManager(this)
        GlobalScope.launch {
            val repos = listOf(
                "github:fennifith/Attribouter",
                "gitlab@salsa.debian.org:reproducible-builds/reproducible-website",
                "gitea@code.horrific.dev:james/git-rest-wrapper",
                "gitlab@code.horrific.dev:james/git-rest-wrapper"
            ).map { client.getRepo(it) }

            recycler?.post {
                recycler?.adapter = RepoAdapter(repos)
            }
        }
    }

}