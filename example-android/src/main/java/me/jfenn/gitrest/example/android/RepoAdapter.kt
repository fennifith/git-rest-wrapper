package me.jfenn.gitrest.example.android

import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import me.jfenn.gitrest.model.Repo

class RepoAdapter(
    val items: List<Repo?>
) : RecyclerView.Adapter<RepoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = items.getOrNull(position) ?: return

        holder.title.text = repo.id

        holder.description.movementMethod = LinkMovementMethod()
        holder.description.text = Html.fromHtml("""
            ${repo.description}<br>
            <i>License: ${repo.license?.name ?: repo.license?.id}</i><br><br>
            <a href="${repo.websiteUrl}">${repo.websiteUrl}</a><br>
            <a href="${repo.url}">${repo.url}</a><br><br>
            <tt>git clone ${repo.gitUrlSsh}</tt>
        """.trimIndent())
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val title: TextView = v.findViewById(R.id.title)
        val description: TextView = v.findViewById(R.id.description)
    }

}