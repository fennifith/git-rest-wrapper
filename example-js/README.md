# example-js

This module demonstrates the use of the bundled JS file in a webpage, without the use of Kotlin/JS.

- `./gradlew :example-js:run`: serve the `index.html` and `gitrest.js` files on `localhost:8080`.
