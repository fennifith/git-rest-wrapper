package me.jfenn.gitrest.service

import me.jfenn.gitrest.model.DelegateResource

object NoCache : Cache {
    override suspend fun set(key: String, value: Any) {}
    override suspend fun <T> get(id: String): T? = null
}