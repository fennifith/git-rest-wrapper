import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.promise
import me.jfenn.gitrest.gitrest
import me.jfenn.gitrest.service.DiskCache
import kotlin.js.Promise

/**
 * NodeJS-usable wrapper class to convert functions/arguments into their JS-compatible versions
 */
class Client(
    config: dynamic
) {

    val client = gitrest {
        if (config?.tokens != null) {
            val tokens = js("Object").entries(config.tokens)
                .unsafeCast<Array<Array<String>>>()
                .map {
                    it[0].unsafeCast<String>() to it[1]
                }.toMap()

            this.providers.forEach {
                it.tokens.putAll(tokens)
            }
        }

        if (config?.cache?.type == "disk") {
            this.cache = DiskCache(this)
        }
    }

    private fun demangle(obj: dynamic) : dynamic {
        val ret: dynamic = js("{}")
        js("Object").entries(obj)
            .unsafeCast<Array<Array<dynamic>>>()
            .forEach {
                val name = it[0].unsafeCast<String>().split("_")[0]
                if ((ret[name] as? String)?.isBlank() != false)
                    ret[name] = it[1]
            }

        return ret
    }

    @JsName("getUser")
    fun getUser(str: String): Promise<dynamic> = GlobalScope.promise {
        client.getUser(str)?.let { demangle(it) }
    }

    @JsName("getRepo")
    fun getRepo(str: String): Promise<dynamic> = GlobalScope.promise {
        client.getRepo(str)?.let { demangle(it) }
    }

    @JsName("getRepoContributors")
    fun getRepoContributors(str: String): Promise<Array<dynamic>?> = GlobalScope.promise {
        client.getRepoContributors(str)?.map { demangle(it) }?.toTypedArray()
    }

    @JsName("getLicense")
    fun getLicense(str: String): Promise<dynamic> = GlobalScope.promise {
        client.getLicense(str)?.let { demangle(it) }
    }

}