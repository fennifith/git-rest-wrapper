package me.jfenn.gitrest.service

import kotlinx.coroutines.await
import me.jfenn.gitrest.model.GitrestConfig
import me.jfenn.gitrest.model.Repo
import kotlin.js.Date
import kotlin.js.Promise
import kotlin.js.jsTypeOf

external fun require(module: String): dynamic

/**
 * Unsafe "disk cache" based on node-persist and JSON.parse.
 */
class DiskCache(
    val config: GitrestConfig,
    val cacheDuration: Long = 864000000 // cache for ~10 days by default
) : Cache {

    private var persist: dynamic = null
    private var promisePersist: Promise<dynamic>? = null

    private suspend fun getPersist() : dynamic {
        if (js("typeof localStorage") != "undefined")
            persist = js("localStorage")

        if (persist == null) {
            persist = require("node-persist")
            promisePersist = (persist.init() as Promise<dynamic>)
        }

        promisePersist?.await()

        return persist
    }

    /**
     * Obtain a "version" string using properties of the Repo class
     * - prop names are obfuscated, so this should be somewhat reliable.
     */
    private fun getVersion() : String {
        val keys: (dynamic) -> Array<String> = js("Object.keys")
        return keys(Repo())[0]
    }

    override suspend fun set(key: String, value: Any) {
        val string = getVersion() + "#" + Date.now() + "#" + JSON.stringify(value)
        (getPersist().setItem(key, string) as Promise<dynamic>).await()
    }

    override suspend fun <T> get(key: String): T? {
        val string = (getPersist().getItem(key) as Promise<String?>).await() ?: return null

        val stringContents = string.split("#", limit = 3)
        if (stringContents.size != 3) return null
        val (version, lastModified, json) = stringContents

        if (version != getVersion())
            return null

        if (Date.now() - lastModified.toLong() < cacheDuration)
            return null

        return if (json.startsWith("{"))
            JSON.parse(json)
        else null
    }

}
