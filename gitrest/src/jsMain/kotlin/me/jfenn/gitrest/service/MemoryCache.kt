package me.jfenn.gitrest.service

import me.jfenn.gitrest.model.DelegateResource

/**
 * Naive caching implementation for JS.
 */
actual class MemoryCache actual constructor(
    val underlyingCache: Cache
) : Cache {

    private val map = HashMap<String, Any>()

    override suspend fun set(key: String, value: Any) {
        map[key] = value
        underlyingCache.set(key, value)
    }

    override suspend fun <T> get(key: String): T? {
        return (map[key] ?: underlyingCache.get<T>(key)) as? T?
    }

}