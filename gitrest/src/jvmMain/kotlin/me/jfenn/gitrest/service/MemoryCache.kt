package me.jfenn.gitrest.service

import me.jfenn.gitrest.model.DelegateResource
import java.lang.ref.ReferenceQueue
import java.lang.ref.WeakReference

/**
 * Naive caching implementation for the JVM that uses WeakReference
 * as a cache "buffer" that can be cleaned up by the GC when necessary.
 */
actual class MemoryCache actual constructor(
    val underlyingCache: Cache
) : Cache {

    private class WeakEntry internal constructor(
        internal val key: String,
        value: Any,
        referenceQueue: ReferenceQueue<Any>) : WeakReference<Any>(value, referenceQueue)

    private val referenceQueue = ReferenceQueue<Any>()
    private val map = HashMap<String, WeakEntry>()

    override suspend fun set(key: String, value: Any) {
        // remove any invalid references
        var weakEntry = referenceQueue.poll() as WeakEntry?
        while (weakEntry != null) {
            map.remove(weakEntry.key)
            weakEntry = referenceQueue.poll() as WeakEntry?
        }

        map[key] = WeakEntry(key, value, referenceQueue)
        underlyingCache.set(key, value)
    }

    override suspend fun <T> get(key: String): T? {
        val weakEntry = map[key]
        weakEntry?.get()?.let {
            return it as? T
        }

        underlyingCache.get<T>(key)?.let {
            set(key, it as Any)
            return it
        }

        return null
    }

}
