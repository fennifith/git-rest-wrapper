import me.jfenn.gitrest.model.ProviderString
import kotlin.test.Test
import kotlin.test.assertEquals

class ProviderStringTest {

    @Test
    fun fromSimpleString() {
        val str = ProviderString("fennifith/Attribouter")

        assertEquals(str.provider, "github")
        assertEquals(str.id, "fennifith/Attribouter")
    }

    @Test
    fun fromProviderString() {
        val str = ProviderString("gitlab:fennifith")

        assertEquals(str.provider, "gitlab")
        assertEquals(str.id, "fennifith")
    }

    @Test
    fun fromContextualProviderString() {
        val str = ProviderString("gitea@code.horrific.dev:james")

        assertEquals(str.provider, "gitea")
        assertEquals(str.hostname, "code.horrific.dev")
        assertEquals(str.id, "james")
    }

}
