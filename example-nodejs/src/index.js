const { Client } = require("git-rest-wrapper-gitrest");

let gitrest = new Client({
    tokens: {
        'a.b': "c"
    }
});

gitrest.getUser("fennifith").then((obj) => {
    console.log(obj);
});

gitrest.getRepo("gitlab@salsa.debian.org:reproducible-builds/reproducible-website").then((obj) => {
    console.log(obj);
});
