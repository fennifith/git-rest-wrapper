# example-nodejs

This module is built using [yarn](https://yarnpkg.com/), to provide an example of JS library usage. The JS target contains its own "proxy" classes to work around property names being mangled by the Kotlin compiler.

- `./gradlew :example-nodejs:run`: execute the `src/index.js` file (with a few example tests/calls using Promises)
