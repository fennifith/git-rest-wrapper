const { Client } = require('./index.js');

let provider = new Client({
    cache: {
        type: 'disk'
    },
    tokens: {
        'a.b': "c"
    }
});

// simple usability test, probably shouldn't break anytime soon...
provider.getUser("fennifith").then((user) => {
    if (user.name === "James Fenn")
        console.log("Test successful!");
    else throw "Something's not quite right...";
});
