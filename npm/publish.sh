#!/bin/bash
set -e

# Update version number in package.json (to match build.gradle def)
PACKAGE_VERSION=$( cd .. && ./gradlew :gitrest:properties | grep -oP '(?<=^version: )[0-9\.]+' )
PACKAGE_JSON=$(jq --arg version "$PACKAGE_VERSION" '.version = $version' package.json)
echo "$PACKAGE_JSON" > package.json

# Build JS file (ignore browser tests...)
( cd .. && ./gradlew :gitrest:build -x :gitrest:jsBrowserTest )

# Copy generated git-rest-wrapper package
cp ../build/js/packages/git-rest-wrapper-gitrest/kotlin/git-rest-wrapper-gitrest.js index.js

# Copy browser bundle
rm -rf bundle && mkdir bundle
cp ../gitrest/build/distributions/* bundle/

BUNDLE_PACKAGES=()
function install_bundled_packages {
  # Re-create node_modules; fill with bundled dependencies
  rm -rf node_modules && mkdir node_modules
  for pkg in ../build/js/packages_imported/*
  do
    pkg_dir=$(ls -d ${pkg}/*)
    pkg_name=${pkg##*/}
    cp -r "$pkg_dir" "node_modules/${pkg_name}"
    BUNDLE_PACKAGES+=("\"$pkg_name\"")
  done
}

install_bundled_packages

# Update 'bundledDependencies' value in package.json
PACKAGE_DEPENDENCIES=$(echo "${BUNDLE_PACKAGES[@]}" | jq -s '.')
PACKAGE_JSON=$(jq --argjson dependencies "$PACKAGE_DEPENDENCIES" '.bundledDependencies = $dependencies' package.json)
echo "$PACKAGE_JSON" > package.json

# Install regular NPM dependencies for testing
NPM_PACKAGES=$(jq '.dependencies | to_entries | map("\(.key)@\(.value|tostring)") | .[]' package.json)
npm install ${NPM_PACKAGES//\"/} --no-package-lock

npm dedupe
node test.js # Make sure the package actually works

# Re-install bundled packages (remove dependencies fetched from npm for testing)
install_bundled_packages

# Copy meta-files from root dir
cp -f ../README.md .
cp -f ../LICENSE .

# Publish the package!
npm publish
